{---------------------------GUI_Ctrl Class--------------------------------
-------------------- Torque Game Engine - GUI Masser ---------------------
---------------------- Roll Game Team 2014 - BLumia ----------------------
   TGuiCtrl is a class for the GUICtrls from TGE's GUI file.
   One TGuiCtrl var has a pointer group to write down the sub GUICtrls.
-------------------------------------------------------------------------}
 unit guictrlclass;
 
 interface

 uses
   SysUtils, Dialogs;

 type ThSizing=(rightH,widthH,leftH,centerH,relativeH);
 type TvSizing=(bottomV,heightV,topV,centerV,relativeV);
 type TBtnType=(PushButton,ToggleButton,RadioButton,RepeaterButton);
 type WTF=(fuck);  //i will never tell u this line is really important for this program
 
 type TdoubleInt = record //Pascal's class is really stupid -- Chirsno
 horiz,vert:integer;
 end;
 
 type TGuiCtrl = class
 public
   profile,functionName,name:string;
   //id:integer;
   parent:integer;
   horizSizing:ThSizing;
   vertSizing:TvSizing;
   position,extent,minExtent:TdoubleInt;
   rpos:TdoubleInt;//Real position -- Chirsno
   rext:TdoubleInt;//Real extent -- BLumia
   visible:boolean;
   visoverride:boolean;//&%#&^(*&)*%$^ -- Chirsno
   variable:WTF;
   command:string;
   altCommand:string;
   accelerator:WTF;
   helpTag:integer;
   text:string;
   groupNum:integer;
   buttonType:TBtnType;
   repeatPeriod,repeatDecay:integer;
   bitmap:string;
   codeLine:integer;
   sub:array of ^TGuiCtrl;
   //Dynamic Fields [Add]
   procedure setParent(a:integer);        //[V][V]
   procedure setProfile(v:string);        //[V][V]
   procedure setFunctionName(v:string);   //[V][V]
   procedure setName(v:string);           //[V][V]
   procedure setHorizSizing(v:string);    //[V][V]
   procedure setVertSizing(v:string);     //[V][V]
   procedure setPosition(v:string);       //[V][V]
   procedure setExtent(v:string);         //[V][V]
   procedure setMinExtent(v:string);      //[V][V]
   procedure setVisible(v:string);        //[V][V]
   procedure setVariable(v:string);       //[X][X]
   procedure setCommand(v:string);        //[V][V]
   procedure setAltCommand(v:string);     //[V][ ]
   procedure setAccelerator(v:string);    //[X][ ]
   procedure setHelpTag(v:string);        //[V][ ]
   procedure setText(v:string);           //[V]
   procedure setGroupNum(v:string);       //[V]
   procedure setButtonType(v:string);     //[V]
   procedure setRepeatPeriod(v:string);   //[V]
   procedure setRepeatDecay(v:string);    //[V]
   procedure setBitmap(v:string);         //[V][V]
   function  viewHorizSizing:string;      //[V][V]
   function  viewVertSizing:string;       //[V][V]
   function  viewPosition:string;         //[V][V]
   function  viewRPos:string;             //[V][V]
   function  viewExtent:string;           //[V][V]
   function  viewRExt:string;             //[V][V]
 end;

implementation

 procedure TGuiCtrl.setParent(a:integer);
 begin
     parent:=a;
 end;

 procedure TGuiCtrl.setProfile(v:string);
 begin
     profile:=v;
 end;

 procedure TGuiCtrl.setName(v:string);
 begin
     name:=v;
 end;

 procedure TGuiCtrl.setFunctionName(v:string);
 begin
     functionName:=v;
 end;
 
 procedure TGuiCtrl.setHorizSizing(v:string);
 var
     temp:ThSizing;
 begin
     temp:=leftH;
     case v of 
     'right':temp:=rightH;
     'width':temp:=widthH;
     'left':temp:=leftH;
     'center':temp:=centerH;
     'relative':temp:=relativeH;
     else showmessage('TGuiCtrl.setHorizSizing error:'+v+#13+'auto set to left');
     end;
     horizSizing:=temp;
 end;

 function TGuiCtrl.viewHorizSizing:string;
 var
     temp:string;
 begin
     temp:='left';
     case horizSizing of
     rightH:temp:='right';
     widthH:temp:='width';
     leftH:temp:='left';
     centerH:temp:='center';
     relativeH:temp:='relative';
     else showmessage('TGuiCtrl.setHorizSizing error,auto set to left');
     end;
     Result:= temp;
 end;
 
 procedure TGuiCtrl.setVertSizing(v:string);
 var
     temp:TvSizing;
 begin
     temp:=topV;
     case v of 
     'bottom':temp:=bottomV;
     'height':temp:=heightV;
     'top':temp:=topV;
     'center':temp:=centerV;
     'relative':temp:=relativeV;
     else ShowMessage('TGuiCtrl.setVertSizing error:'+v+#13+'auto set to top');
     end;
     vertSizing:=temp;
 end;

 function TGuiCtrl.viewVertSizing:string;
 var
     temp:string;
 begin
     temp:='bottom';
     case vertSizing of
     bottomV:temp:='bottom';
     heightV:temp:='height';
     topV:temp:='top';
     centerV:temp:='center';
     relativeV:temp:='relative';
     else
     ShowMessage('TGuiCtrl.setVertSizing error,auto set to bottom');
     end;
     Result:= temp;
 end;
 
 procedure TGuiCtrl.setPosition(v:string);
 var
     tempIntA,tempIntB,i,isLeftFull:integer;    
 begin
     tempIntA:=0;
     tempIntB:=0;
     isLeftFull:=0;
     for i :=1 to length(v) do
     begin
           if (V[i] in['0'..'9',' '])then
            begin
                if (v[i] in['0'..'9'])then   //if (s[i] in['0'..'9','.'])then
                begin
	             if (isLeftFull=0) then
                     tempIntA:= tempIntA*10 + strtoint(v[i])
		     else
       	             tempIntB:= tempIntB*10 + strtoint(v[i]);
	         end else isLeftFull:=1;
	     end;
      end;
      //showmessage('horiz '+inttostr(tempIntA)+' vert '+inttostr(tempIntB));
      position.horiz:=tempIntA;
      position.vert:=tempIntB;
 end; 

 function TGuiCtrl.viewPosition:string;
 var
     temp:string;
 begin
     temp:=inttostr(position.horiz)+' '+inttostr(position.vert);
     Result:= temp;
 end;

 function TGuiCtrl.viewRPos:string;
 var
     temp:string;
 begin
     temp:=inttostr(rpos.horiz)+' '+inttostr(rpos.vert);
     Result:= temp;
 end;

 procedure TGuiCtrl.setExtent(v:string);
 var
     tempIntA,tempIntB,i,isLeftFull:integer;    
 begin
     tempIntA:=0;
	 tempIntB:=0;
	 isLeftFull:=0;
 for i :=1 to length(v) do
 begin
	 if (v[i] in['0'..'9',' '])then begin
         if (v[i] in['0'..'9'])then   //if (v[i] in['0'..'9','.'])then
         begin
	         if (isLeftFull=0) then
                 tempIntA:= tempIntA*10 + strtoint(v[i])
		     else
        		 tempIntB:= tempIntB*10 + strtoint(v[i]);
	     end else isLeftFull:=1;
	 end;
 end;
 extent.horiz:=tempIntA;
 extent.vert:=tempIntB; 
 end;  

 function TGuiCtrl.viewExtent:string;
 var
     temp:string;
 begin
     temp:=inttostr(extent.horiz)+' '+inttostr(extent.vert);
     Result:= temp;
 end;

 function TGuiCtrl.viewRExt:string;
 var
     temp:string;
 begin
     temp:=inttostr(rext.horiz)+' '+inttostr(rext.vert);
     Result:= temp;
 end;

 procedure TGuiCtrl.setMinExtent(v:string);
 var
     tempIntA,tempIntB,i,isLeftFull:integer;    
 begin
     tempIntA:=0;
	 tempIntB:=0;
	 isLeftFull:=0;
 for i :=1 to length(v) do
 begin
	 if (v[i] in['0'..'9',' '])then begin
         if (v[i] in['0'..'9'])then   //if (v[i] in['0'..'9','.'])then
         begin
	         if (isLeftFull=0) then
                 tempIntA:= tempIntA*10 + strtoint(v[i])
		     else
        		 tempIntB:= tempIntB*10 + strtoint(v[i]);
	     end else isLeftFull:=1;
	 end;
 end;
 minExtent.horiz:=tempIntA;
 minExtent.vert:=tempIntB; 
 end;  
 
 procedure TGuiCtrl.setVisible(v:string);  
 begin
     if (v='1') then visible:= true else visible:= false;
 end;
 
 procedure TGuiCtrl.setVariable(v:string);  
 begin
     //i really do NOT know what does [variable] do.
 end;

 procedure TGuiCtrl.setCommand(v:string);
 begin
     command:=v;
 end;
 
 procedure TGuiCtrl.setAltCommand(v:string);
 begin
     altCommand:=v;
 end;
 
 procedure TGuiCtrl.setAccelerator(v:string);  
 begin
     //i really do NOT know what does [Accelerator] do.
 end;
 
 procedure TGuiCtrl.setHelpTag(v:string);
 begin
     helpTag:=strtoint(v);
 end; 
 
 procedure TGuiCtrl.setText(v:string);
 begin
     text:=v;
 end; 
 
 procedure TGuiCtrl.setGroupNum(v:string);
 begin
     groupNum:=strtoint(v);
 end; 
 
 procedure TGuiCtrl.setButtonType(v:string);   
 var
     temp:TBtnType;
 begin
     temp:=PushButton;
     case v of 
     'PushButton':temp:=PushButton;
     'ToggleButton':temp:=ToggleButton;
     'RadioButton':temp:=RadioButton;
     'RepeaterButton':temp:=RepeaterButton; 
     else
     ShowMessage('TGuiCtrl.setButtonType error,auto set to PushButton');
     end;
     buttonType:=temp;
 end;
 
 procedure TGuiCtrl.setRepeatPeriod(v:string);
 begin
     repeatPeriod:=strtoint(v);
 end;
 
 procedure TGuiCtrl.setRepeatDecay(v:string);
 begin
     repeatDecay:=strtoint(v);
 end; 
 
 procedure TGuiCtrl.setBitmap(v:string);
 begin
     bitmap:=v;
 end; 
 
end.
