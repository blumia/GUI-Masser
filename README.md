#GUI Masser

GUI Masser 是为Torque Game Engine所使用的gui文件所设计的简易可视化GUI编辑器。

##用途

GUI Masser旨在用来预览Torque3D以及TorqueGameEngine游戏引擎所使用的gui文件，并支持导出一个gui文件，以便于GUI文件的可视化预览和编辑，以解决TorqueGameEngine中预览不完整和预览错位的问题。

##用法和注意事项

本工具可以用来预览Torque3D以及TorqueGameEngine游戏引擎所使用的gui文件，并支持导出一个gui文件，但请注意，本软件 ** 不能用来直接编辑现有的gui ** ，因为输出时，部分属性会被忽视输出，比如command属性。所以，仅建议您使用本软件对gui进行 *预览* 和 *简单调试* 而 __不进行__ 输出操作。

使用本软件前，您需要先将本软件放置在正确的位置，您可以查看您的gui文件中bitmap属性所使用的相对位置，本软件就应当放置在那个位置，此路径一般是:  ModName/client/ui/.。如果路径放置不正确，可能会导致图片无法正常显示等问题。

您需要先点击左上角的【SelectFile】选择文件，当提示【it is done】时文件加载完成。此时点击下方DrawGUI即可看到gui的大致结构和图形界面。

您可以在右侧对单独的控件或容器进行编辑，每次编辑需要通过点击【ApplyChanges】应用变更。

 - 下方的新控件按钮需要在右上角树形图中选择任意一项后才可点击。
 - 下方的【ContainerMode】是为了显示每个容器的边框准备的，选中此项后点击任意一个绘制GUI按钮生效。
 - 下方的【Help】按钮可能并不能给你多大帮助，不明白的话，还是再看一次这段文字好了~所以就别点【Help】按钮了。
 
Sizing部分的处理我也做了一些，目前的Sizing支持的内容有：
 - 一般对齐（如right/bottom左对齐和上对齐）
 - 居中对齐（center）
 - 长宽对齐（height/width）
 - 自动缩放对齐（relative）
如出现未能识别的其他对齐方式，目前会按照一般对齐进行处理（自适应对齐似乎比较高级所以没做）
   
##继续支持？

由于是早期作品，并且很多代码写的还是较为不合理的。所以此软件已不打算继续支持。此后可能会搞同类功能的软件，但也会是其他语言重写而不是用这个的代码了。
恭喜你竟然坚持看到了这里。给跪了Orz。祝使用愉快~ 